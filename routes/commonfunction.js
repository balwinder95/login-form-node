var sendResponse = require('./sendResponse');



exports.checkBlank = function (arr) {
  return (checkBlank(arr));
};

function checkBlank(arr) {
  var arrlength = arr.length;
  for (var i = 0; i < arrlength; i++) {
    if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
      return 1;
    }
  }
  return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

  var checkBlankData = checkBlank(manValues);

  if (checkBlankData) {
    sendResponse.parameterMissingError(res);
  }
  else {
    callback(null);
  }
}

exports.encrypt = function (text) {

  var crypto = require('crypto');
  var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
  var crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

exports.authenticateAccessTokenAndReturnExtraData = function (res, accesstoken, arr, callback) {

  var sql = 'SELECT `user_id`';
  arr.forEach(function (entry) {
    sql += ',' + entry;
  });
  sql += ' FROM `user_info`';
  sql += ' WHERE `access_token`=? LIMIT 1';
  dbConnection.Query(res, sql, [accesstoken], function (result) {

    if (result.length > 0) {
      return callback(null, result);
    } else {
      sendResponse.invalidAccessTokenError(res);
    }
  });
}

exports.checkLoginStatus = function (res, accessToken, callback) {
  var sql = 'SELECT `user_id` FROM `user_info` WHERE `access_token`=? AND `login_status`=1 LIMIT 1';
  dbConnection.Query(res, sql,[accessToken], function (result) {
    if (result.length > 0) {
      callback(null);
    } else {
      sendResponse.invalidAccessTokenError(res);
    }
  });
}