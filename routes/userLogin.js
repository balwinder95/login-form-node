var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');

exports.registerFromEmail = function (req, res) {
	var firstName = req.body.first_name;
  var lastName = req.body.last_name;
  var email = req.body.email;
  var mobile = req.body.mobile;
  var password = req.body.password;
  var userType = req.body.user_type;
  var deviceType = req.body.device_type;
  var deviceToken = req.body.device_token;
  var appVersion = req.body.app_version;

  var manValues = [firstName, email, mobile, password, deviceType, appVersion];

  async.waterfall([
    function(callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    },
    function(callback) {
      checkEmailAvailability(res, email, callback);
    }], function() {

      var loginTime = new Date();
      var accessToken = commonfunction.encrypt(email + loginTime);
      var encryptPassword = md5(password);
      var sql = 'INSERT INTO `user_info`(`access_token`, `first_name`, `last_name`, `email`, `mobile`, `password`, `user_type`, `device_type`, `device_token`, `updated_at`,`app_version`) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
      dbConnection.Query(res, sql, [accessToken, firstName, lastName, email, mobile, encryptPassword, userType, deviceType, deviceToken, loginTime, appVersion], function (response) {

        var userID = response.insertId;
        var data = {access_token: accessToken, first_name: firstName, last_name: lastName, mobile: mobile, user_id: userID, email: email};
        sendResponse.sendSuccessData(data, res);

      });
  });
};


exports.emailLogin = function(req, res) {
  var email = req.body.email;
  var password = req.body.password;
  var userType = req.body.user_type;
  var deviceType = req.body.device_type;
  var deviceToken = req.body.device_token;
  var appVersion = req.body.app_version;

  var manValues = [email, password, deviceType, appVersion];

  async.waterfall([
    function(callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    },
    function(callback) {
      checkEmailPasswordAndUserType(res, email, password, userType, callback);
    }], function(err, response) {

    var userID = response[0].user_id;
    var loginTime = new Date();
    var accessToken = commonfunction.encrypt(email + loginTime);

    var data = {access_token: accessToken};
    updateUserParams(res, accessToken, deviceToken, deviceType, loginTime, appVersion, userID);
    sendResponse.sendSuccessData(data, res);
    });
};


exports.userLogout = function(req, res) {
  var accessToken = req.body.access_token;
  var manValues = [accessToken];

  async.waterfall([
    function(callback) {
      commonfunction.checkBlankWithCallback(res, manValues, callback);
    }, function(callback) {
      commonfunction.checkLoginStatus(res, accessToken, callback);
    }, function(callback) {
      var extraDataNeeded = [];
      commonfunction.authenticateAccessTokenAndReturnExtraData(res, accessToken, extraDataNeeded, callback);

    }], function (err, userInfo) {

      var userID = userInfo[0].user_id;

      var sql = 'UPDATE `user_info` SET `device_type`=?,`device_token`=?, `login_status`=? WHERE `user_id`=? limit 1';
      dbConnection.Query(res, sql, [0, 0, 0, userID], function (response) {

        var data = {"message": "Logged out successfully."};
        sendResponse.sendSuccessData(data, res);
    });
  });
};


function checkEmailPasswordAndUserType(res, email, password, userType, callback) {

  var encryptPassword = md5(password);

  var sql = 'SELECT `user_id`, `access_token`,`first_name`,`last_name`,`mobile` FROM `user_info` WHERE `email`=? and `password`=? and `user_type`=? LIMIT 1';
  dbConnection.Query(res, sql, [email, encryptPassword, userType], function (response) {

    if (!response.length) {
        var error = 'The email or password you entered is incorrect.';
        sendResponse.sendErrorMessage(error, res);
    } else {
        callback(null, response);
    }
  });
}


function updateUserParams(res, accessToken, deviceToken, deviceType, loginTime, appVersion, userID) {

  var sql = 'UPDATE `user_info` SET `access_token`=?, `device_type`=?,`device_token`=?,`app_version`=?,`updated_at`=?, `login_status`=1 WHERE `user_id`=? limit 1';
  dbConnection.Query(res, sql, [accessToken, deviceType, deviceToken, appVersion, loginTime, userID], function (result) {
  });
}


function checkEmailAvailability(res, email, callback) {

  var sql = 'SELECT `user_id` FROM `user_info` WHERE `email`=? limit 1';
  dbConnection.Query(res, sql, [email], function (response) {

    if (response.length > 0) {
      var error = 'Email already exists.';
      sendResponse.sendErrorMessage(error, res);
    } else {
      callback();
    }
  });
}