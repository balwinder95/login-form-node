var express = require('express');
var router = express.Router();
var userLogin = require('./userLogin');

/* GET test page. */
router.get('/test', function (req, res) {
  res.render('test');
});

router.post('/registerFromEmail', userLogin.registerFromEmail);
router.post('/emailLogin', userLogin.emailLogin);
router.post('/userLogout', userLogin.userLogout);

module.exports = router;